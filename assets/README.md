# Birthday Calendar
Random collection of strings and assets.


## Onboarding claims
Never
Miss birthdays again. Get birthdays for all your social networks right inside Google Calendar.

Universal
Supports Facebook, LinkedIn, Xing, Skype and all other apps which sync contacts with your Android device.

Remember
Enable reminder alarms and decide which contacts you want to see. You'll get notifications for all of them.
